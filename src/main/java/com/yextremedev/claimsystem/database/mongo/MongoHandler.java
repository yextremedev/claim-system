package com.yextremedev.claimsystem.database.mongo;

import com.mongodb.MongoClient;
import com.yextremedev.claimsystem.chunk.dao.ChunkDao;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

public class MongoHandler {

    private final MongoClient client;
    private final Morphia morphia;
    private final Datastore datastore;
    private final ChunkDao dao;

    public MongoHandler() {
        this.client = new MongoClient();
        this.morphia = new Morphia();
        this.datastore = this.morphia.createDatastore(this.client, "chunks");
        this.dao = new ChunkDao(this.datastore);
    }

    public MongoClient getClient() {
        return client;
    }

    public Morphia getMorphia() {
        return morphia;
    }

    public Datastore getDatastore() {
        return datastore;
    }

    public ChunkDao getDao() {
        return dao;
    }
}
