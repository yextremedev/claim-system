package com.yextremedev.claimsystem.database;

import com.yextremedev.claimsystem.chunk.ChunkData;
import com.yextremedev.claimsystem.chunk.dto.ChunkDto;
import com.yextremedev.claimsystem.database.mongo.MongoHandler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DatabaseManager {

    private final MongoHandler handler;

    public DatabaseManager() {
        this.handler = new MongoHandler();
    }

    public MongoHandler getHandler() {
        return handler;
    }

    public List<ChunkData> getAllSavedChunks() {
        List<ChunkData> chunkData = new ArrayList<>();

        for (ChunkDto chunkDto : handler.getDao().find().asList()) {
            chunkData.add(new ChunkData(chunkDto));
        }

        return chunkData;
    }

    public void saveChunk(ChunkDto chunkDto) {
        handler.getDao().save(chunkDto);
    }

    public void saveAll(Set<ChunkData> data) {
        Set<ChunkDto> dto = new HashSet<>();

        for (ChunkData chunkData : data) {
            ChunkDto chunkDto = new ChunkDto();

            chunkDto.setX(chunkData.getX());
            chunkDto.setZ(chunkData.getZ());
            chunkDto.setOwner(chunkData.getOwner());
            chunkDto.setWorld(chunkData.getWorldName());

            dto.add(chunkDto);
        }

        dto.forEach(chunkDto -> handler.getDao().save(chunkDto));
    }
}
