package com.yextremedev.claimsystem.commands.registration;

import com.yextremedev.claimsystem.commands.Command;
import com.yextremedev.claimsystem.utils.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;

import java.util.logging.Level;

public class CommandRegister {

    public void register(final Command command) {
        ((CraftServer) Bukkit.getServer()).getCommandMap().register(command.getName(), new org.bukkit.command.Command(command.getName()) {
            @Override
            public boolean execute(CommandSender commandSender, String s, String[] strings) {
                try {
                    if (commandSender instanceof Player) {
                        command.execute((Player) commandSender);
                    } else {
                        commandSender.sendMessage(Messages.NOT_PLAYER);
                    }
                } catch (Exception e) {
                    commandSender.sendMessage(Messages.ERROR_OCCURRED_PLAYER);
                    Bukkit.getLogger().log(Level.WARNING, String.format(Messages.ERROR_OCCURRED_CONSOLE, commandSender.getName()), e);
                    return false;
                }
                return true;
            }
        });
    }

}
