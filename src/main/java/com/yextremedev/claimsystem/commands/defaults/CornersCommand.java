package com.yextremedev.claimsystem.commands.defaults;

import com.yextremedev.claimsystem.chunk.corners.CornerManager;
import com.yextremedev.claimsystem.commands.Command;
import com.yextremedev.claimsystem.main.ClaimSystem;
import com.yextremedev.claimsystem.utils.Messages;
import org.bukkit.entity.Player;

public class CornersCommand extends Command {


    public CornersCommand() {
        super();
    }

    @Override
    public String getName() {
        return "corners";
    }

    @Override
    public void execute(Player player) {
        CornerManager manager = ClaimSystem.getChunkManager().getCornerManager();

        if (manager.isRunning(player.getName())) {
            manager.stop(player.getName());
            player.sendMessage(Messages.CHUNK_CORNERS_STOP);
        } else {
            manager.play(player);
            player.sendMessage(Messages.CHUNK_CORNERS_START);
        }
    }
}
