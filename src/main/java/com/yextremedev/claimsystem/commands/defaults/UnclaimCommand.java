package com.yextremedev.claimsystem.commands.defaults;

import com.yextremedev.claimsystem.chunk.ChunkData;
import com.yextremedev.claimsystem.commands.Command;
import com.yextremedev.claimsystem.main.ClaimSystem;
import com.yextremedev.claimsystem.utils.Messages;
import org.bukkit.entity.Player;

public class UnclaimCommand extends Command {

    @Override
    public String getName() {
        return "unclaim";
    }

    @Override
    public void execute(Player player) {
        ChunkData data = ClaimSystem.getChunkManager().getChunkData(player.getLocation().getChunk());
        if (data.isClaimed()) {
            if (data.getOwner().equals(player.getName())) {
                data.removeOwner();
                player.sendMessage(Messages.CHUNK_UNCLAIMED);
            } else {
                player.sendMessage(String.format(Messages.CHUNK_NOT_OWNER, data.getOwner()));
            }
        } else {
            player.sendMessage(Messages.CHUNK_NOT_CLAIMED);
        }
    }

}
