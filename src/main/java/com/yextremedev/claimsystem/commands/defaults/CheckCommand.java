package com.yextremedev.claimsystem.commands.defaults;

import com.yextremedev.claimsystem.chunk.ChunkData;
import com.yextremedev.claimsystem.commands.Command;
import com.yextremedev.claimsystem.main.ClaimSystem;
import com.yextremedev.claimsystem.utils.Messages;
import org.bukkit.entity.Player;

public class CheckCommand extends Command {

    @Override
    public String getName() {
        return "check";
    }

    @Override
    public void execute(Player player) {
        ChunkData data = ClaimSystem.getChunkManager().getChunkData(player.getLocation().getChunk());
        if (data.isClaimed()) {
            player.sendMessage(String.format(Messages.CHUNK_CLAIMED_BY, data.getX(), data.getZ(), data.getOwner()));
        } else {
            player.sendMessage(String.format(Messages.CHUNK_NOT_CLAIMED_BY, data.getX(), data.getZ()));
        }
    }

}
