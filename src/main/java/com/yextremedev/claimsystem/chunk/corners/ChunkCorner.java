package com.yextremedev.claimsystem.chunk.corners;

import com.yextremedev.claimsystem.chunk.ChunkData;
import org.bukkit.Location;

import java.util.HashSet;
import java.util.Set;

public class ChunkCorner {

    private final ChunkData chunkData;

    public ChunkCorner(ChunkData chunkData) {
        this.chunkData = chunkData;
    }

    public ChunkData getChunkData() {
        return chunkData;
    }

    public Set<Location> getCorners(double y) {

        Set<Location> corners = new HashSet<>();

        for (int i = 0; i != 15; i++) {
            corners.add(getChunkData().get(i, y, 0));
            corners.add(getChunkData().get(15, y, i));
            corners.add(getChunkData().get(15 - i, y, 15));
            corners.add(getChunkData().get(0, y, 15 - i));
        }

        return corners;
    }

}
