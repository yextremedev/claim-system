package com.yextremedev.claimsystem.chunk.corners.task;

import com.yextremedev.claimsystem.chunk.ChunkData;
import com.yextremedev.claimsystem.main.ClaimSystem;
import com.yextremedev.claimsystem.utils.ParticleHelper;
import org.bukkit.entity.Player;

import java.util.TimerTask;

public class CornerTask extends TimerTask {

    private final Player player;
    private boolean running;

    CornerTask(Player player) {
        this.player = player;
        this.running = true;
    }

    public Player getPlayer() {
        return player;
    }


    public boolean isRunning() {
        return running;
    }

    @Override
    public boolean cancel() {
        this.running = false;
        return super.cancel();
    }

    @Override
    public void run() {
        ChunkData data = ClaimSystem.getChunkManager().getChunkData(player.getLocation().getChunk());
        data.getCorners(player.getLocation().getY() + 2.0D).forEach(location -> ParticleHelper.sendParticle(player, location));
    }

}
