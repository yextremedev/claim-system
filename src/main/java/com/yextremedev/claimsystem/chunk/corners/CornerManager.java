package com.yextremedev.claimsystem.chunk.corners;

import com.yextremedev.claimsystem.chunk.corners.task.CornerTimer;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CornerManager {

    private final Map<String, CornerTimer> runnables;

    public CornerManager() {
        this.runnables = new HashMap<>();
    }

    public Collection<CornerTimer> getRunnables() {
        return runnables.values();
    }

    private CornerTimer getTimer(String player) {
        return runnables.get(player);
    }

    public boolean isRunning(String player) {
        CornerTimer timer = getTimer(player);
        return timer != null && timer.isRunning();
    }

    public void play(Player player) {
        if (!isRunning(player.getName())) {
            runnables.put(player.getName(), new CornerTimer(player).start());
        }
    }

    public void stop(String player) {
        CornerTimer timer = getTimer(player);
        if (timer != null && timer.isRunning()) {
            timer.stop();
        }
        runnables.remove(player);
    }

    public void disable() {
        runnables.forEach((player, cornerTimer) -> {
            if (cornerTimer != null && cornerTimer.isRunning()) {
                cornerTimer.stop();
            }
        });
        runnables.clear();
    }
}
