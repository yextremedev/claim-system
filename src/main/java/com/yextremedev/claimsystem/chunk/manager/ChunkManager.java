package com.yextremedev.claimsystem.chunk.manager;

import com.yextremedev.claimsystem.chunk.ChunkData;
import com.yextremedev.claimsystem.chunk.corners.CornerManager;
import org.bukkit.Chunk;
import org.bukkit.World;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ChunkManager {

    private final Set<ChunkData> chunkData;

    private final CornerManager cornerManager;

    public ChunkManager(Set<ChunkData> defaults) {
        this.chunkData = defaults;
        this.cornerManager = new CornerManager();
    }

    public ChunkManager(List<ChunkData> defaults) {
        this(new HashSet<>(defaults));
    }

    public CornerManager getCornerManager() {
        return cornerManager;
    }

    public Set<ChunkData> getAllLoaded() {
        return chunkData;
    }

    public ChunkData getChunkData(String world, int x, int z) {
        for (ChunkData data : this.getAllLoaded()) {
            if (data.equals(world, x, z)) {
                return data;
            }
        }
        return create(world, x, z);
    }

    public ChunkData getChunkData(World world, int x, int z) {
        return getChunkData(world.getName(), x, z);
    }

    public ChunkData getChunkData(Chunk chunk) {
        return getChunkData(chunk.getWorld(), chunk.getX(), chunk.getZ());
    }

    private ChunkData create(String world, int x, int z) {
        ChunkData data = new ChunkData(world, x, z);
        this.getAllLoaded().add(data);
        return data;
    }


}
