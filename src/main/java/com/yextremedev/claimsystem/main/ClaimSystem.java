package com.yextremedev.claimsystem.main;

import com.yextremedev.claimsystem.chunk.manager.ChunkManager;
import com.yextremedev.claimsystem.commands.defaults.CheckCommand;
import com.yextremedev.claimsystem.commands.defaults.ClaimCommand;
import com.yextremedev.claimsystem.commands.defaults.CornersCommand;
import com.yextremedev.claimsystem.commands.defaults.UnclaimCommand;
import com.yextremedev.claimsystem.commands.registration.CommandRegister;
import com.yextremedev.claimsystem.database.DatabaseManager;
import com.yextremedev.claimsystem.main.bukkit.System;

public class ClaimSystem {

    private static volatile System system;
    private static volatile ChunkManager chunkManager;
    private static volatile CommandRegister commandRegister;
    private static volatile DatabaseManager databaseManager;

    public static DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public static System getSystem() {
        return system;
    }

    public static ChunkManager getChunkManager() {
        return chunkManager;
    }

    public static CommandRegister getCommandRegister() {
        return commandRegister;
    }

    public synchronized void enable() {
        system = new System();
        commandRegister = new CommandRegister();
        databaseManager = new DatabaseManager();
        chunkManager = new ChunkManager(databaseManager.getAllSavedChunks());

        new ClaimCommand();
        new CheckCommand();
        new UnclaimCommand();
        new CornersCommand();
    }

    public void disable() {
        chunkManager.getCornerManager().disable();
        databaseManager.saveAll(chunkManager.getAllLoaded());
    }

}
